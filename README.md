Ce projet a pour but l'élaboration d'un bulletin nul à caractère revendicatif pour les élections françaises.

Le modèle présenté est au format A4. Le fichier bulletin.xcf peut être ouvert et exploité par [Gimp][1], un logiciel libre d'infographie.

Sous licence [CC-BY-SA][2].

[1]: https://www.gimp.org/
[2]: https://creativecommons.org/licenses/by-sa/4.0/
